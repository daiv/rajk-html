var videoURL = 'img/intro.mp4';
var req = new XMLHttpRequest();
req.open('GET', videoURL, true);
req.responseType = 'blob';

req.onreadystatechange = function () {
    if (req.DONE === req.readyState) {
        if (req.status === 200 || req.status === 304) {
            var videoBlob = req.response;
            var vid = URL.createObjectURL(videoBlob);
            document.getElementById('video').setAttribute("src", vid);
        } else {
            console.log("video file " + req.responseURL + " not available")
        }
    }
};
req.send(null);